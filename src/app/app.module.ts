import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatSliderModule } from '@angular/material/slider';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatFormFieldModule } from '@angular/material/form-field';
import { HttpClientModule } from '@angular/common/http';
import { MatInputModule } from '@angular/material/input';
import { FoodComponent } from './services/food/food.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { CountryComponent } from './services/country/country.component';
import { IngredientComponent } from './services/ingredient/ingredient.component';
import { SnotifyModule, SnotifyService, ToastDefaults } from 'ng-snotify';
import { MatButtonModule } from '@angular/material/button';
import { ModalDialogComponent } from './modals/modal-dialog/modal-dialog.component';
import { TasteTheWorldComponent } from './taste-the-world/taste-the-world.component';
import { SearchComponent } from './services/search/search.component';



@NgModule({
  declarations: [
    AppComponent,
    FoodComponent,
    CountryComponent,
    IngredientComponent,
    TasteTheWorldComponent,
    ModalDialogComponent,
    SearchComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    MatSliderModule,
    BrowserAnimationsModule,
    NgbModule,
    MatFormFieldModule,
    HttpClientModule,
    MatInputModule,
    NgbModule,
    MatButtonModule,
    SnotifyModule

  ],
  providers: [{ provide: 'SnotifyToastConfig', useValue: ToastDefaults},
  SnotifyService
],
  bootstrap: [AppComponent],
  entryComponents:[ModalDialogComponent],
})
export class AppModule { }
