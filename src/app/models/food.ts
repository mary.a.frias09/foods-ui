import { Country } from './country';

export class Food {
    id: number; 
    foodName: string;
    country: Country
}