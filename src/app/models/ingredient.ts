import { Food } from './food';

export class Ingredient {
  id: number;
  ingredientName:string;
  food: Food;
}
