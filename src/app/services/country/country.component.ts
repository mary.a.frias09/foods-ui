import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CountryService } from '../country.service';
import { Country } from 'src/app/models/country';
import { SnotifyService } from 'ng-snotify';
import { DataTransferService } from '../data-transfer.service';

@Component({
  selector: 'app-country',
  templateUrl: './country.component.html',
  styleUrls: ['./country.component.css']
})
export class CountryComponent implements OnInit {
  countryForm: FormGroup;
  
  constructor(
    private fb: FormBuilder,
    public snotify: SnotifyService,
    private countrySvc: CountryService,
    public dataTransfer: DataTransferService) { }

  ngOnInit(): void {
    this.countryForm = this.createCountryForm();
  }

  createCountryForm() {
  return this.fb.group({
    id:[''],
    countryName:['']
  })
  }

  createCountry() {
    const country = new Country();
    country.countryName = this.countryForm.controls.countryName.value;
    this.countrySvc.createFood(country)
    .toPromise()
    .then((res) => {
      console.log(res);
      // fix snotify
      // this.snotify.successs("Country was created")
      this.dataTransfer.createFood = true;
      this.dataTransfer.country = res;
    })
  }
}
