import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Food } from 'src/app/models/food';
import { SnotifyService } from 'ng-snotify';
import { DataTransferService } from '../data-transfer.service';


@Component({
  selector: 'app-food',
  templateUrl: './food.component.html',
  styleUrls: ['./food.component.css']
})
export class FoodComponent implements OnInit {
  foodForm: FormGroup;
  newFood = false;

  constructor(
    private fb: FormBuilder,
    private snotify: SnotifyService,
    private foodSvc: ApiService,
    public dataTransfer: DataTransferService
  ) {}

  ngOnInit(): void {
    this.foodForm = this.createFoodForm();
  }

  createFoodForm() {
   return this.fb.group({
      id: [''],
      foodName: [''],
      country: [null],
    });
  }

  createFood() {
    if(this.dataTransfer.country) {
      const food =  new Food();
      food.foodName = this.foodForm.controls.foodName.value;
      food.country = this.dataTransfer.country;
      this.foodSvc.createFood(food)
      .toPromise()
      .then((res)=> {
        this.dataTransfer.food = res;
        this.dataTransfer.createIngredient = true;
      })
    }
  }
}
