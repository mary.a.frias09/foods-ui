import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Food } from '../models/food';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  createFood(food: Food) {
    return this.http.post(environment.url + 'foods/create', food) as Observable<Food>;
  }

  updateFood(food: Food) {
    return this.http.put(environment.url + 'foods/update', food) as Observable<Food>;
  }

  deleteFood(id: number) {
    return this.http.delete(environment.url + 'foods/delete/' + id) as Observable<Food>;
  }

  getAllFoods() {
    return this.http.get(environment.url + 'foods/all') as Observable<Food>;
  }
}
