import { Component, OnInit } from '@angular/core';
import { Ingredient } from 'src/app/models/ingredient';
import { FormBuilder, FormGroup } from '@angular/forms';
import { IngredientService } from '../ingredient.service';
import { DataTransferService } from '../data-transfer.service';


import { SnotifyService } from 'ng-snotify';

@Component({
  selector: 'app-ingredient',
  templateUrl: './ingredient.component.html',
  styleUrls: ['./ingredient.component.css']
})
export class IngredientComponent implements OnInit {
  ingredientForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private snotify: SnotifyService,
    private ingredientSvc: IngredientService,
    public dataTransfer: DataTransferService
  ) {}

  ngOnInit(): void {
    this.ingredientForm = this.createIngredientForm();
  }

  createIngredientForm() {
    return this.fb.group({
      id: [''],
      ingredientName: [''],
      food: [null],
    });
  }

  createIngredient() {
    if (this.dataTransfer.food && this.dataTransfer.country) {
      const ingredient = new Ingredient();
      ingredient.ingredientName = this.ingredientForm.controls.ingredientName.value;
      ingredient.food = this.dataTransfer.food;
      this.ingredientSvc
        .createIngredient(ingredient)
        .toPromise()
        .then((res) => {
          this.dataTransfer.ingredient = res;
          this.ingredientForm.controls.ingredientName.setValue('');
        });
    }
  }

  finish() {
    this.dataTransfer.createCountry = false;
    this.dataTransfer.createFood = false;
    this.dataTransfer.createIngredient = false;
    this.ngOnInit();
  }
}
