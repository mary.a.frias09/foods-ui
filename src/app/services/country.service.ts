import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Country } from '../models/country';

@Injectable({
  providedIn: 'root'
})
export class CountryService {

  constructor(private http: HttpClient) { }

  createFood(country: Country) {
    return this.http.post(environment.url + 'countries/create', country) as Observable<Country>;
  }

  updateFood(country: Country) {
    return this.http.put(environment.url + 'countries/update', country) as Observable<Country>;
  }

  deleteFood(id: number) {
    return this.http.delete(environment.url + 'countries/delete' + id) as Observable<Country>;
  }

  getAllCountries() {
    return this.http.get(environment.url + 'countries/all') as Observable<Country>;
  }

}
