import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Ingredient } from '../models/ingredient';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class IngredientService {

  constructor(private http: HttpClient) { }

  createIngredient(ingredient: Ingredient) {
    return this.http.post(environment.url + 'ingredients/create/', ingredient) as Observable<Ingredient>;
  }

  createIngredientsList(list: Ingredient[]) {
    return this.http.post(environment.url + 'ingredients/create-ingredients-list', list) as Observable<Ingredient[]>;
  }

  updateIngredient(ingredient: Ingredient) {
    return this.http.put(environment.url + 'ingredients/update', ingredient) as Observable<Ingredient>;
  }

  deleteIngredient(id: number) {
    return this.http.delete(environment.url + 'ingredients/delete/' + id) as Observable<Ingredient>;
  }

  getAllIngredients() {
    return this.http.get(environment.url + 'ingredients/all') as Observable<Ingredient>;
  }
}
