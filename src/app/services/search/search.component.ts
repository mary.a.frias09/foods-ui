import { Component, OnInit } from '@angular/core';
import { SearchService } from '../search.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  constructor(private searchSvc: SearchService) { }

  ingredientsResults: any[] = [];
  foodName: string;
  country: string;

  ngOnInit(): void {
  }

  searchIngredientsBy(country, food) {
    this.searchSvc.searchIngredientsByCountryAndFood(country, food)
    .toPromise()
    .then((res) => {console.log(res)
      this.country = res;
      this.ingredientsResults = res;
    })
  }

  // ****Create a Method that will add an ingredient to an existing dish,
  // you can add it by searching a dish and having button to add to existing dish
  // upon searching*****.
}
