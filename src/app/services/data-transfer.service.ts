import { Injectable } from '@angular/core';
import { Country } from '../models/country';
import { Food } from '../models/food';
import { Ingredient } from '../models/ingredient';

@Injectable({
  providedIn: 'root'
})
export class DataTransferService {

  createCountry = false;
  createFood = false;
  createIngredient = false;

  country: Country;
  food: Food;
  ingredient:Ingredient;

  constructor() { }
}
