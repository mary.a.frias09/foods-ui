import { Component, OnInit } from '@angular/core';
import { DataTransferService } from '../services/data-transfer.service';


@Component({
  selector: 'app-taste-the-world',
  templateUrl: './taste-the-world.component.html',
  styleUrls: ['./taste-the-world.component.css']
})
export class TasteTheWorldComponent implements OnInit {


  new = false;
  constructor(public dataTranfer: DataTransferService) { }

  ngOnInit(): void {
  }

  addNew() {
    this.dataTranfer.createCountry = true;
  }

  cancel() {
    this.dataTranfer.createCountry = false;
  }

}
